from FSM.state import *

@singleton
class State11(State):

    # @debugState
    def state_constructor_output(self):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self):
        'Outputs executed once when the state is leaved'

    # @debugState
    def next_state_compute(self, **params):
        'Compute the next state'
        if self.guard_pend(["FSM","FSM2","STATE","Roesti", "phase"], phase.DESTRUCTOR):
            return "Choki"
        else:
            return "Praline"

@singleton
class State12(State):
    
    # @debugState
    def state_constructor_output(self):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self):
        'Outputs executed once when the state is leaved'

    
    # @debugState
    def next_state_compute(self, **params):
        'Compute the next state'
        return "Choki"

@singleton
class State13(State):
    
    # @debugState
    def state_constructor_output(self):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self):
        'Outputs executed once when the state is leaved'

    
    # @debugState
    def next_state_compute(self, **params):
        'Compute the next state'
        return "Prune"
