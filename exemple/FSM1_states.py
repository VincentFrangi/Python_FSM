from FSM.state import *

@singleton
class State1(State):

    # @debugState
    def state_constructor_output(self, *args, **kwargs):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self, *args, **kwargs):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self, *args, **kwargs):
        'Outputs executed once when the state is leaved'

    # @debugState
    def next_state_compute(self, *args, **kwargs):
        'Compute the next state'
        if self.guard_pend(["FSM", "FSM2", "STATE", "Roesti", "phase"],  phase.DESTRUCTOR):
           return "macro10"
        else:
           return "Biscotte"

@singleton
class State2(State):
    
    # @debugState
    def state_constructor_output(self, *args, **kwargs):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self, *args, **kwargs):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self, *args, **kwargs):
        'Outputs executed once when the state is leaved'

    
    # @debugState
    def next_state_compute(self, *args, **kwargs):
        'Compute the next state'
        return "Biscotte"