import asyncio

from FSM1_states import *
from FSM2_states import *
from FSM.manager import *
from FSM.state_machine import *
from Macro10_states import *
import json


def main():
    # Create an async loop
    loop = asyncio.get_event_loop()

    # --------------------------------------------------------
    FSM_manager1 = FSM_manager()

    # Create FSMs
    FSM1 = State_machine(name="FSM1", FSM_manager=FSM_manager1)
    FSM2 = State_machine(name="FSM2", FSM_manager=FSM_manager1)

    # Create states
    state_1 = State1(supervisor=FSM1, name="Biscotte", id=0)
    state_1.guard_create(["FSM", "FSM2", "STATE", "Roesti", "phase"], phase.DESTRUCTOR)
    state_1.guard_create(["FSM", "FSM2", "STATE", "Jambon", "phase"], phase.DESTRUCTOR)
    state_1.guard_create(["FSM", "FSM2", "STATE", "Jambon", "phase"], phase.CONSTRUCTOR)

    state_2 = State2(supervisor=FSM1, name="Fromage", id=1)
    state_3 = State3(supervisor=FSM2, name="Jambon", id=3)
    state_4 = State4(supervisor=FSM2, name="Roesti", id=4)

    macro10 = macro(name="macro10", supervisor = FSM1,)
    macro10.guard_create(["FSM", "FSM2", "STATE", "Roesti", "phase"], phase.DESTRUCTOR)
    macro10.guard_create(["FSM", "FSM2", "call"], 3)

    state_11 = State11(supervisor=macro10, name="Praline", id=5)
    state_11.guard_create(["FSM", "FSM2","STATE","Roesti", "phase"], phase.DESTRUCTOR)
    
    state_12 = State12(supervisor=macro10, name="Choki", id=6)
    state_13 = State13(supervisor=macro10, name="Prune", id=7)
    
    macro10.entrance_departure_set(entrance="Praline", departur={"Choki" : "Biscotte", "Prune" : "Fromage"})
    

    # --------------------------------------------------------

    try:
        # loop.create_task(periodic(1)) #Create a periodic task
        FSM1.run(initState=state_1, loop=loop, period=2)
        FSM2.run(initState=state_3, loop=loop, period=2)

        loop.run_forever()  # Start the loop forever5

    except (KeyboardInterrupt, SystemExit):
        print('Stop Loop')
        FSM_manager1.info()
        with open('result.json', 'w') as fp:
            print('write result.json')    
            json.dump(FSM_manager1.events, fp, sort_keys=True, indent=4)
            fp.close()


if __name__ == '__main__':
    main()
