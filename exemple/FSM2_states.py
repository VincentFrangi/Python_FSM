from FSM.state import *

@singleton
class State3(State):
    #TODO wrap debug state for class
    # @debugState
    def state_constructor_output(self, *args, **kwargs):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self, *args, **kwargs):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self, *args, **kwargs):
        'Outputs executed once when the state is leaved'

    # @debugState
    def next_state_compute(self, *args, **kwargs):
        'Compute the next state'
        return "Roesti"

@singleton
class State4(State):
    
    # @debugState
    def state_constructor_output(self, *args, **kwargs):
        'Constructor of the state executed once when the state is called'
        
    # @debugState
    def state_current_output(self, *args, **kwargs):
        'Outputs executed when the state is called'

    # @debugState
    def state_destructor_output(self, *args, **kwargs):
        'Outputs executed once when the state is leaved'

    
    # @debugState
    def next_state_compute(self, *args, **kwargs):
        'Compute the next state'
        return "Jambon"
        