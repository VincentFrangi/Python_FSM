# Python FSM

* Vincent Frangi

## To run the exemple:

frist run setup

## Functionalities

* State Structure
    * Constructor
    * Current
    * Destructor
* State Machine
    * Attach itself to an asyncio periodic loop
    * Observe all his states (keep the last event of each states)
    * Can be observed by other states machines
