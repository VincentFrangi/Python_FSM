import json
import os

from FSM.observer import *


class FSM_manager(Relay):
    def __init__(self):
        Observer.__init__(self)
        Subject.__init__(self)
        self.type = su_type.MANAGER
        self.guard_manager = Guard_manager()
        self.attach(self.guard_manager)

    def notify(self, subject, event):
        'Be notified'
        self.last_event.clear()
        self.last_event[subject.type.name]={}
        self.last_event[subject.type.name][subject.name] = event
        mergePath(self.last_event, self.events)

        self.notify_observers(self.last_event)
        self.info()  # Print change

    def info(self):
        def cls(): return os.system('clear')
        cls()
        print("-----------------FSM---------------------")
        print(json.dumps(self.events, sort_keys=True, indent=4))

        print("-----------------Guard------------------")
        print(json.dumps(self.guard_manager.guards, sort_keys=True, indent=4))


def getPathValue(path, dico):
    '''Recursiv get the value of a path, if exist return value
    if wrong path return None
    if phase if wrong return None'''
    try:
        try:
            val = dico.get(path, None)  # return the value of the key
            return val  # Path is righ
        except TypeError:  # if path is a dict
            for key in path:  # get the keys
                val = getPathValue(path[key], dico[key])
            return val
    except KeyError:
        return None  # Path is wrong


def setPathValue(path, dico, value):
    '''Recursiv get the value of a path, if exist return value
    if wrong path return None
    if phase if wrong return None'''
    try:
        try:
            val = dico.get(path, None)  # return the value of the key
            if val is not None:
                dico[path] = value  # Set the guard to true
            return dico[path]
        except TypeError:  # if path is a dict
            val = None
            for key in path:  # get the keys
                val = setPathValue(path[key], dico[key], value)
            return val
    except KeyError:
        return None  # Path is wrong


def mergePath(path, dico):

    if type(path) is dict:                  # Continue exploring
        for key, v in path.items():         # write all the path include
            if type(v) is dict:             # test if it's not the last item in path
                try:
                    mergePath(v, dico[key])
                except (KeyError, TypeError):   # 
                    dico[key] = {}              # Create dict to copy path in
                    mergePath(v, dico[key])
            else:                               # if it's the last item in path it's a value
                 dico[key] = v

                


class Guard_manager(Observer):
    def __init__(self):
         # self.guards[consumer.name][path][..][path][phase] = False
        self.guards = {}
        Observer.__init__(self)

    def guard_create(self, consumer, path, ev):
        try:
            d = self.guards[consumer.name]
        except KeyError:
            self.guards[consumer.name] = {}
            d = self.guards[consumer.name]
        for i in path:  # Add path to dict (recursion)
            try:
                d = d[i]
            except KeyError:
                d[i] = {}
                d = d[i]
        if(path[-1]== "phase"):
            d[ev.name] = False     # Add the event and affect the status
        elif(path[-1] == "call"):
            d[ev] = False
        else:
            d[ev.name] = False #TODO Unknown type

        # self.info()  # Print change

    def notify(self, subject, event):
        for consumer in self.guards:  # Go through the guards
            # Set the values to true
            setPathValue(event, self.guards[consumer], True)

    def pend_fromDict(self, consumer, path):
        try:
            # Set the values to false
            val = setPathValue(path, self.guards[consumer], False)
            if val is None:
                print("Wrong path or Phase")
                return False
            else:
                return val
        except KeyError:
            print("consumer has no dedicated guards")
            return False

    def pend_request(self, consumer, path, ev):
        try:
            d = self.guards[consumer.name]
        except KeyError:
            print("consumer has no dedicated guards")
            return False
        for i in path:  # Add path to dict (recursion)
            try:
                d = d[i]
            except KeyError:
                print("Wrong path")
                return False
        try:
            g = d[ev.name]
            d[ev.name] = False   # Add the event and affect the status
            return g
        except KeyError:
            print("Wrong Phase")
            return False
