# state.py
# Vincent Frangi 2018

from enum import Enum

from colorama import *

from FSM.observer import *

'Debug decorator'


def debugState(func):
    def debug_wrapper(self):
        print("-----------------")
        print("Attached supervisor: " + str(self.supervisor))
        print("Cycle: " + str(self.supervisor.cycle))
        #print("called by: " + str(self.calling_class))
        print("Name: " + self.name)
        print("Phase: " + str(self.phase))
        print("function called:")
        print(Fore.RED, end='')
        print(func.__name__)
        print(Style.RESET_ALL, end='')
        print("-----------------")
        return func(self)
    return debug_wrapper


def singleton(cls):
    instances = {}  # Disct of class

    def getinstance(**args):
        if cls not in instances:    # Look in the list
            instances[cls] = cls(**args)  # Create a new instace in the dict

        # Notify observer when phase change
        # instances[cls].last_event = {}
        # instances[cls].last_event["Instance"] = str(instances[cls])
        # instances[cls].notify_observers(instances[cls].last_event)
        return instances[cls]       # Share the instance in the dict
    return getinstance


class phase(Enum):
    IDLE = 0
    CONSTRUCTOR = 1
    CURRENT = 2
    DESTRUCTOR = 3

    def __str__(self):
        return str(self.name)


class State(Subject):

    def __init__(self, supervisor, name, id=-1):
        Subject.__init__(self)
        self.type = su_type.STATE
        self.name = name
        self.id = id               # ID of the state
        self.supervisor = supervisor             # Parent State machine
        self.supervisor.attachState(self)  # Attach itself to the statemachine
        # Attach the state machine to the observer list
        self.attach(supervisor)
        self.phase = phase.IDLE    # Init phase and notify Observer

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, phase):
        self._phase = phase
        # Notify observer when phase change
        self.last_event = {}
        self.last_event["phase"] = self.phase.name
        self.notify_observers(self.last_event)

    def guard_create(self, path, ev):
        self.supervisor.FSM_manager.guard_manager.guard_create(self, path, ev)

    def guard_pend(self, path, ev):
        return self.supervisor.FSM_manager.guard_manager.pend_request(self, path, ev)

    def __str__(self):
         return str(self.name)

    def state_constructor_output(self, *args, **kwargs):
        'Constructor of the state executed once when the state is called'
        raise NotImplementedError

    def state_current_output(self, *args, **kwargs):
        'Outputs executed when the state is called'
        raise NotImplementedError

    def state_destructor_output(self, *args, **kwargs):
        'Outputs executed once when the state is leaved'
        raise NotImplementedError

    def next_state_compute(self, *args, **kwargs):
        'Compute the next state'
        return self
