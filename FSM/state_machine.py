# state_machine.py
# Vincent Frangi 2018

import asyncio

from FSM.observer import *
from FSM.state import *


class State_machine(Relay):

    def __init__(self, name, FSM_manager=None):
        # Init superclass Observer (Observer states)
        Relay.__init__(self)
        self.type = su_type.FSM

        if FSM_manager is not None:         # FSM Manager is optionnal
            self.attach(FSM_manager)
            self.FSM_manager = FSM_manager

        self.name = name
        self.is_running = False
        self.call = 0
        self.states = {}                    # A dict with all the states
        self.currentState = "Not set"

    def __str__(self):
        return self.name

    @property
    def currentState(self):
        return self._currentState

    @currentState.setter
    def currentState(self, state):
        self._currentState = state
        # Notify observer when phase change
        self.last_event = {}
        self.last_event["currentState"] = state
        self.notify_observers(self.last_event)

    def attachInitState(self, initState):
        
        self.attachState(initState)             # Attach to the list of states
        self.currentState = initState.name
    
        # Inform with event
        self.last_event.clear()
        self.last_event["initState"] = initState.name
        self.notify_observers(self.last_event)

    def attachState(self, state):
        'Attach a state to the state machine reference list'
        if not state.name in self.states:         # Test if the inistate isn't in the list
            self.states[state.name] = state    # Add to the list
            self.events[state.name] = {}    # Create a place in the dict #TODO usefull ?

            # Inform with event
            #self.last_event.clear()
            #self.last_event[state.name] = "attached"
            # self.last_event[state.name] = {}
            #self.last_event[state.type.name]["Status"] = "attached"
            #self.notify_observers(self.last_event)

    def readtransitionTable(self): #TODO implement load and backup
        'Read the transition table'
        raise NotImplementedError

    def run(self, initState, loop, period=1):
        'Run the state machine, create the asyncio task'
        self.period = period
        
        self.attachInitState(initState)

        self.nextState = self.currentState
        self.states[self.currentState].phase = phase.CONSTRUCTOR  # Wake current state

        # Attach to asyncio
        self.loop = loop
        self.loop.create_task(self.task_StateMachine())

        # Inform with event
        self.last_event.clear()
        self.last_event = {}
        self.last_event["run"] = True
        self.last_event["period"] = self.period        
        self.notify_observers(self.last_event)

    def stop(self): #TODO test
        'Stop the state machine'
        self.states[self.currentState].phase = phase.IDLE
        self.is_running = False

        #TODO detach from asyncio ?

        # Inform with event
        self.last_event.clear()
        self.last_event["run"] = False
        self.notify_observers(self.last_event)

    def task_StateMachine(self):
        'Task of Ascyncio periodic call'
        while True:
            self.state_outputs_exec()      # Execute the output and change the phase
            self.call += 1                 # Increment number of cycle counter

            #TODO replace with a function
            #I Inform with event
            self.last_event.clear()
            self.last_event = {}
            self.last_event["call"] = self.call
            self.notify_observers(self.last_event)

            yield from asyncio.sleep(self.period)   # Wait for a "period"

    def load_nextState(self):
        'Load the next state'
        self.nextState = self.states[self.currentState].next_state_compute()
        if self.nextState not in self.states:
            raise KeyError
        #I Inform with event
        self.last_event.clear()
        self.last_event = {}
        self.last_event["next State"] = self.nextState
        self.notify_observers(self.last_event)

    def state_outputs_exec(self):
        '''Execute the output of the state depending on the phase
        and computation of the new phase'''
        current = self.states[self.currentState]

        if current.phase is phase.CONSTRUCTOR:
            current.state_constructor_output()    # Execute the output
            current.phase = phase.CURRENT         # Change the Phase

        elif current.phase is phase.CURRENT:
            current.state_current_output()         # Execute the output
            # Compute the next State
            self.load_nextState()

            # Check if the next state is a different state
            if self.currentState != self.nextState:
                current.phase = phase.DESTRUCTOR  # Change the phase
            else:
                current.phase = phase.CURRENT     # Keep the current phase

        elif current.phase == phase.DESTRUCTOR:
            current.state_destructor_output()     # Execute output
            current.phase = phase.IDLE            # Change Phase to IDLE
            self.states[self.nextState].phase = phase.CONSTRUCTOR        # Wake the next state
            
            # Load the next state to current
            self.currentState = self.nextState

        elif current.phase == phase.IDLE:
            assert 0, "State in IDLE phase"
        else:
            assert 0, "State phase unknown"

class macro(State, State_machine):
    def __init__(self, name, supervisor, id=-1):
        State_machine.__init__(self, name)
        self.type = su_type.MACRO
        # Observer.__init__(self)
        # Subject.__init__(self)
        # self.name = name

        self.FSM_manager = supervisor.FSM_manager
        self.attach(supervisor)
        self.supervisor = supervisor
        self.supervisor.attachState(self)  # Attach itself to the statemachine
        self.id = id
        self.phase = phase.IDLE    # Init phase and notify Observer
        self.stateList = []
        self.cycle = 0
        self.currentState = None
        self.exitpoints = {}

    def entrance_departure_set(self, entrance, departur):
        
        #TODO check if in the list
        self.currentState = entrance

        for exitpoint, direction in departur.items():
            #self.attachState(exitpoint)
            if not exitpoint in self.exitpoints:
                self.exitpoints[exitpoint] = {}
                self.exitpoints[exitpoint]["Direction"] = direction
                        # Inform with event
        self.last_event.clear()
        self.last_event = {}
        self.last_event["exitpoint"] = self.exitpoints        
        self.notify_observers(self.last_event)



    def run(self):
        'Run the state machine'

        if self.currentState is None:
            raise NotImplementedError

        self.nextState = self.currentState
        self.states[self.currentState].phase = phase.CONSTRUCTOR  # Wake current state

        # Inform with event
        self.last_event.clear()
        self.last_event = {}
        self.last_event["run"] = True
        self.notify_observers(self.last_event)

    def task_StateMachine(self):
        'Tak of Ascyncio periodic call'
        self.state_outputs_exec()      # Execute the output and change the phase
        self.call += 1                 # Increment number of cycle counter
        
        #TODO replace with a function
        # Inform with event
        self.last_event.clear()
        self.last_event = {}
        self.last_event["call"] = self.call
        self.notify_observers(self.last_event)

    def state_constructor_output(self, *args, **kwargs):
        'Run the state machine, create the asyncio task'
        pass
        self.run()

    def state_current_output(self, *args, **kwargs):
        self.task_StateMachine()

    def state_destructor_output(self, *args, **kwargs):
        self.stop()

    def next_state_compute(self, *args, **kwargs):
        if self.currentState in self.exitpoints:
            return self.currentState
        return self.name
