'Observer design pattern (wikipedia)'

from enum import Enum


class su_type(Enum):
    MANAGER = 0
    FSM = 1
    STATE = 2
    MACRO = 3
    SUBJECT = 4
    OBSERVER =5
    RELAY = 6

    def __str__(self):
        return str(self.name)

class Subject:
    def __init__(self):
        self.type = su_type.SUBJECT
        self.__observers = []
        self.last_intern_event = {}

    def attach(self, observer):
        'Attach a new observer'
        self.__observers.append(observer)

    def detach(self, observer):
        'Detach an observer'
        self.__observers.remove(observer)

    def notify_observers(self, event):
        'Notify all the observers'
        self.last_intern_event.clear()
        self.last_intern_event = dict(event)

        for observer in self.__observers:
            observer.notify(self, self.last_intern_event)


'Look the subjets'


class Observer:
    def __init__(self):
        self.type = su_type.OBSERVER
        self.last_event = {}
        self._events = {}

    @property
    def events(self):
        return self._events

    @events.setter
    def events(self, val):
        raise PermissionError

    def notify(self, subject, event):
        'Be notified'
        self.last_event.clear()
        self.last_event[subject.type.name] = {}
        self.last_event[subject.type.name][subject.name] = event  
        self.events = {**self.events, **self.last_event}

    def add_subject(self, subject):
        'Add a subject to watch'
        subject.attach(self)  # Attach itself to a subject


class Relay(Subject, Observer):
    def __init__(self):
        self.type = su_type.RELAY
        Subject.__init__(self)
        Observer.__init__(self)

    def notify(self, subject, event):
        self.last_event.clear()
        self.last_event[subject.type.name] = {}
        self.last_event[subject.type.name][subject.name] = event        
        self.notify_observers(self.last_event)
